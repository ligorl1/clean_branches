<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;

class SearchBranchCommand extends Command {

    protected function configure() {
        $this
                // the name of the command (the part after "app/console")
                ->setName('app:search-branch')
                // the short description shown while running "php app/console list"
                ->setDescription("Поиск веток на bitbucket.org\n\t\t\t\t\t[app:search-branch name_dir], name_dir - каталог где лежат репозитории")
                ->setHelp("Поиск веток на bitbucket.org...")
                ->addArgument('param', InputArgument::REQUIRED, 'Репозиторий');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        $dir = $input->getArgument('param');

        if (!is_dir($dir)) {
            $output->writeln("Каталог $dir не существует");
        } else {

            $ignor = array("master", "dev");
            $output->writeln(shell_exec("clear"));
            $dirs = shell_exec("bash start.sh $dir");
            $dir_repository = explode("\n", $dirs);
            foreach ($dir_repository as $key => $value) {
                if (strlen($value) > 0) {
                    if (is_dir("$dir/$value")) {
                        $list_branch[$value] = shell_exec("bash search.sh $dir/$value");
                    }
                }
            }
            if (!empty($list_branch)) {
                foreach ($list_branch as $key => $value) {
                    $branch = explode("\n", $value);
                    /* Название репозитария $key */
                    $output->writeln("Репозиторий: " . $key);
                    foreach ($branch as $k => $val) {
                        if ($k >= 5 && $k < (count($branch) - 5) && !strpos($val,"git pull")) {
                            $val = trim($val);
                            /* Название ветки $val */
                            if (!preg_match("/^\d{5}/i", $val)) {
                                $flag_ignor = false;
                                foreach ($ignor as $v) {
                                    if (strcmp($v, $val) == 0) {
                                        $flag_ignor = true;
                                    }
                                }
                                if (!$flag_ignor) {
                                    $output->writeln($val);
                                }
                            }
                        }
                    }
                }
            }else{
                $output->writeln("Каталог $dir пустой");
            }
        }
    }

}
